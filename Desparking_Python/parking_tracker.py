from parse_rest.datatypes import Object
from parse_rest.connection import register
from parse_rest.connection import ParseBatcher
import random

register("vkEL45uCiPrxQ66aoT7peWVwTA59mpAsVTCAEkeS", "vXhnNiwUAekdyD62XoMyKMzDwGfFxvueyyhtVSMV", master_key="mjbiLLS9Pwcbu5RdfPkqecBZ5BAvghPPg7kzKJ7D")

class ParkSpace(Object):
    pass

spaces = ParkSpace.Query.all()
 
def saveStatuses():
    ParseBatcher().batch_save(spaces)
    
spaces[0].isOccupied = True if random.randrange(0, 1) == 1 else False

saveStatuses()