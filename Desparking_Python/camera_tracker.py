import cv2
import numpy as np
import pickle
import os
import parse_rest 
import parse_rest.connection
import parse_rest.datatypes
import datetime
import pprint

CAR_PRESENCE_THRESHOLD = 70
CORNERS_ALPHA = 0.001

class ParkSpace(parse_rest.datatypes.Object):
   	pass


class DummyParseClient:

	def __init__(self):
		self.result = {}

	def register(self):
		pass

	def get_spaces(self):
		pass

	def set_is_occupied(self, name, is_occupied, count):
		row, col = [int(x) for x in name.split(",")]
		self.result[(row, col)] = (is_occupied, count)

	def update_spaces(self):
		print '-----', datetime.datetime.now(), '-----'
		pprint.pprint(self.result)

class ParseClient:

	def __init__(self):
		self.park_spaces = []
		self.park_spaces_map = {}

		self.prev_result = None
		self.result = {}
		self.failed = False

	def register(self):
		print 'Registering to the Parse API'
		parse_rest.connection.register("vkEL45uCiPrxQ66aoT7peWVwTA59mpAsVTCAEkeS", "vXhnNiwUAekdyD62XoMyKMzDwGfFxvueyyhtVSMV", master_key="mjbiLLS9Pwcbu5RdfPkqecBZ5BAvghPPg7kzKJ7D")
		print 'Done'

	def get_spaces(self):
		print 'Loading park places...'
		self.park_spaces = ParkSpace.Query.all()
		print 'Done'

		try:
			self.failed = False
			for park_space in self.park_spaces:
				key = (park_space.parkRow, park_space.parkColumn)
				self.park_spaces_map[key] = park_space
		except:
			self.failed = True
			print 'Error: The park spaces could not be loaded.'

	def set_is_occupied(self, name, is_occupied, count):
		row, col = [int(x) for x in name.split(",")]
		self.result[(row, col)] = is_occupied

		park_space = self.park_spaces_map[(row, col)]
		park_space.isOccupied = is_occupied

	def update_spaces(self):
		if self.failed:
			return

		print '-----', datetime.datetime.now(), '-----'
		pprint.pprint(self.result)

		print self.prev_result != self.result

		if self.prev_result != self.result:
			print 'Sending data'
			try:
				parse_rest.connection.ParseBatcher().batch_save(self.park_spaces)
				print 'Done\n'
			except:
				print 'Sending failed'

		self.prev_result = self.result
		self.result = {}


class PolygonsCollection:

	def __init__(self, images_count):
		self.polygons = {index: [] for index in range(images_count)}
		self.point_to_polygon = {index: None for index in range(images_count)}

	def current(self, image_index):
		if len(self.polygons[image_index]) > 0:
			return self.polygons[image_index][-1]

		return None

	def update_point_to_polygon(self, image_index, image_shape):
		height, width, depth = image_shape

		for y in range(height):
			for x in range(width):
				point = (x, y)

				for polygon in self.polygons[image_index]:
					if polygon.contains_point(point):
						self.point_to_polygon[image_index][point] = polygon
						break

	def update(self, image_index, image_shape, corners):
		if not self.point_to_polygon[image_index]:
			self.point_to_polygon[image_index] = {}
			self.update_point_to_polygon(image_index, image_shape)

		for polygon in self.polygons[image_index]:
			polygon.car_present = False
			polygon.corners_inside_count = 0

		for corner in corners:
			polygon = self.point_to_polygon[image_index].get(corner, None)

			if polygon:
				polygon.corners_inside_count += 1
				polygon.car_present = polygon.corners_inside_count >= CAR_PRESENCE_THRESHOLD

		for polygon in self.polygons[image_index]:
			self.parse_client.set_is_occupied(polygon.name, polygon.car_present, polygon.corners_inside_count)

	def add(self, image_index, polygon):
		self.polygons[image_index].append(polygon)

	def draw(self, image_index, image):
		for polygon in self.polygons[image_index]:
			polygon.draw(image)

	def save(self):
		file = open('polygons', 'wb')
		pickle.dump(self, file)
		file.close()

	@staticmethod
	def last_or_new(images_count):
		polygons_col = None

		if os.path.isfile('polygons'):
			file = open('polygons', 'r')
			polygons_col = pickle.load(file)
			file.close()
		else:
			polygons_col = PolygonsCollection(images_count)

		return polygons_col

	
class Polygon:

	def __init__(self):
		self.points = []
		self.points_nparray = None
		self.car_present = False
		self.name = ""
		self.corners_inside_count = 0
		self.finished = False

	def add_point(self, point):
		self.points.append(point)
		self.points_nparray = np.array(self.points)

	def contains_point(self, point):
		if len(self.points) < 2:
			return False

		return cv2.pointPolygonTest(self.points_nparray, point, False) >= 0

	def draw(self, image):
		if len(self.points) > 0:
			cv_data = self.points_nparray

			color = (0, 255, 0)
			if self.car_present:
				color = (0, 0, 255)

			cv2.polylines(image, [cv_data], True, color)


class Capturer:

	def __init__(self, arg):
		parts = arg.split(':')

		self.video_capture = cv2.VideoCapture(int(parts[0]))
		self.bounds = [int(x) for x in parts[1].split(',')]
		self.detect_corners = True
		self.equalize_hist = False
		self.show_points = False

	def read(self):
		ok, image = self.video_capture.read()
		height, width, depth = image.shape

		top, left, bottom, right = self.bounds
		bottom = height - 1 if bottom == 0 else bottom
		right = width - 1 if right == 0 else right

		image = image[top:bottom, left:right]
		corners = None

		if self.detect_corners:
			gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
			if self.equalize_hist:
				gray = cv2.equalizeHist(gray)
			gray = np.float32(gray)

			dst = cv2.cornerHarris(gray, 2, 3, 0.04)

			corners_predicate = dst > CORNERS_ALPHA * dst.max()
			corners = np.where(corners_predicate)

			if self.show_points:
				image[corners_predicate] = [0, 255, 0]

		return image, corners

	def release(self):
		self.video_capture.release()


class Application:

	def __init__(self, args):
		self.capturers = [Capturer(arg) for arg in args]
		self.windows = ['Video' + str(i) for i in range(len(self.capturers))]

		self.draw_enabled = True
		self.detect_corners = False
		self.equalize_hist = False
		self.show_points = False

		self.parse_client = ParseClient()
		self.last_update_time = datetime.datetime.now()

		self.polygons_col = PolygonsCollection.last_or_new(len(self.capturers))
		self.polygons_col.parse_client = self.parse_client

	def make_mouse_callback(self, image_index):
		def mouse_call_back(event, x, y, flags, param):
			if self.detect_corners:
				return

			if event == cv2.EVENT_LBUTTONUP:
				polygon = self.polygons_col.current(image_index)

				if not polygon or polygon.finished:
					polygon = Polygon()
					self.polygons_col.add(image_index, polygon)

				polygon.add_point([x, y])
			elif event == cv2.EVENT_RBUTTONUP:
				polygon = self.polygons_col.current(image_index)

				if polygon:
					polygon.name = raw_input('Enter polygon name: ')
					polygon.finished = True
				
		return mouse_call_back

	def process_keys(self):
		ch = cv2.waitKey(20)

	 	if ch == 27:
	 		return False
	 	elif ch == ord('d'):
	 		self.draw_enabled = not self.draw_enabled
	 	elif ch == ord('c'):
	 		self.detect_corners = not self.detect_corners
	 	elif ch == ord('s'):
	 		self.polygons_col.save()
	 	elif ch == ord('e'):
	 		self.equalize_hist = not self.equalize_hist
	 	elif ch == ord('p'):
	 		self.show_points = not self.show_points

	 	return True


	def setup(self):
		for index, window in enumerate(self.windows):
			cv2.namedWindow(window)
			cv2.setMouseCallback(window, self.make_mouse_callback(index))

		self.parse_client.register()
		self.parse_client.get_spaces()
	

	def run(self):
		while(True):
			for index, capturer in enumerate(self.capturers):
				capturer.detect_corners = self.detect_corners
				capturer.equalize_hist = self.equalize_hist
				capturer.show_points = self.show_points

				image, corners = capturer.read();

				if self.detect_corners:
					self.polygons_col.update(index, image.shape, zip(corners[1], corners[0]))

				if self.draw_enabled:
					self.polygons_col.draw(index, image)

				cv2.imshow(self.windows[index], image)

			if self.detect_corners:
				time = datetime.datetime.now()

				if (time - self.last_update_time).seconds > 3:
					self.parse_client.update_spaces()
					self.last_update_time = time

	 		if not self.process_keys():
	 			break

	def destroy(self):
		for capturer in self.capturers:
	 		capturer.release();

		cv2.destroyAllWindows()

app = Application(["0:100,480,-100,-100", "2:100,100,-200,-350"])
app.setup()
app.run()
app.destroy()

