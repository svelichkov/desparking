//
//  DataManager.swift
//  Desparking
//
//  Created by Velislava Yanchina on 05/08/2014.
//  Copyright (c) 2014 despark. All rights reserved.
//

import Foundation

class DataManager : NSObject {
    
    
    override init() {
        Parse.setApplicationId("vkEL45uCiPrxQ66aoT7peWVwTA59mpAsVTCAEkeS", clientKey: "dNgKf3VA18rp8HCf66GsOqPPJFxaBkkYH0mPTwFl")
        super.init()
    }
    
    func getFreeSpotsCount (completionClosure: ((NSNumber) -> Void)?) {
        var query = PFQuery(className: "ParkSpace")
        var parkSpace = PFObject(className: "ParkSpace")
        var freeParkSpotsCount = 0;
        
        query.findObjectsInBackgroundWithBlock {
            (objects: [AnyObject]!, error: NSError!) -> Void in
            if let unwrappedError : NSError = error? {
                // Log details of the failure
//                NSLog("Error: %@ %@", unwrappedError, unwrappedError.userInfo)
                if ((completionClosure) != nil) {
                    completionClosure!(0)
                }
            } else {
                // The find succeeded.
                NSLog("Successfully retrieved \(objects.count) spaces.")
                // Do something with the found objects
                for object in objects {
                    let occupied: NSNumber! = object.objectForKey("isOccupied") as NSNumber
                    if (!occupied.boolValue) {
                        freeParkSpotsCount++
                    }
                }
                if ((completionClosure) != nil) {
                    completionClosure!(freeParkSpotsCount)
                }
                
 
            }
        }
    }
    
}